module Collectionable
  extend ActiveSupport::Concern

  included do

  end


  def work_on_collection(mylist, params = {})

    object_params = mylist[0].class.attribute_names   # object attributes

    # attributes

    params.each_pair do |key,value|
      if object_params.include?(key)
        mylist = mylist.where(key => value)
      end
    end

    # sort

    if params.include?('sort') && object_params.include?(params.fetch(:sort).to_s.gsub('-',''))
      puts 'SORT INCLUDED AND VALID'
      if params.fetch(:sort).to_s.start_with?('-')
        mylist = mylist.order("#{params.fetch(:sort).to_s.gsub('-','')} DESC")
      else
        mylist = mylist.order("#{params.fetch(:sort).to_s} ASC")
      end
    end

    mylist

  end




end
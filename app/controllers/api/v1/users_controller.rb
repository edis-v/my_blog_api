class Api::V1::UsersController < ApplicationController
  include Collectionable
  skip_before_action :authenticate, only: [:index, :show, :create]
  load_and_authorize_resource

  resource_description do
    formats [:json]
    short 'API for managing User Profile'
    error 404, 'Resource not found'
    error 422, 'Unprocessable entity'
    error 401, 'Not authorized for this action'
  end

  def_param_group :user do
    param :user, Hash, required: true, desc: 'User information' do
      param :email, String, desc: 'User email', required: true
      param :password, String, desc: 'User password', required: true
      param :admin, [true, false], desc: 'If true user is admin. Only admin can create new admins'
    end
  end



  api :GET, '/users', 'Get all users'
  description 'API for fetching all users'
  param :page, String, desc: 'Page parameter for pagination. Default value: 1'
  param :per_page, String, desc: 'How many results will be returned with every page. Default value: 5'
  param :sort, String, desc: 'Sorting records based on any User attribute. \'-\' sign before attr for DESC order.'
  param :fields, String, desc: 'Which user attributes API will return. Divide attributes with \',\' sign.'
  param :id, String, desc: 'Select user based on attribute value.'
  param :email, String, desc: 'Select user based on attribute value.'
  param :admin, String, desc: 'Select users based on attribute value.'
  example "'users': [{'id':1, 'email':'user1@mail.com', 'admin':'false', 'links':'api/v1/users/1'}, {...}]"

  def index

    @users = work_on_collection(@users, request.query_parameters)

    @users = paginate @users, per_page: params[:per_page] || 5

    render json: @users, fields: params[:fields]

  end

  api :GET, '/users/:id', 'Get specific user'
  description 'API for fetching user with specific ID'
  example "'user': {'id':1, 'email':'some@mail.com', 'admin':true, 'links':'api/v1/users/1'}"

  def show
    render json: @user, fields: params[:fields]
  end

  api :POST, '/users', 'Create user'
  description <<-EOS
    === Create User
      This API is used to create new user
    ==== Authentication required
      Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example "{'user':{'id':1,'email':'some@mail.com','admin':false, 'links':'api/v1/users/1', 'auth_token':'xxxxxxxx'}"
  param_group :user

  def create

    if @user.save
      render json: @user, serializer: CompleteUserSerializer, status: :created, location: [:api, :v1, @user]

    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  api :PUT, '/users/:id', 'Update user'
  description <<-EOS
    == Update User Profile
     This API is used to update the user profile details.
    === Authentication required
     Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'
  param_group :user

  def update

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  api :DELETE, '/users/:id', 'Delete user'
  description  <<-EOS
    == Delete User Profile
     This API is used to delete the user profile details.
    === Authentication required
     Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'
  def destroy
    @user.destroy

    head :no_content
  end

  private

    def user_params
      params.require(:user).permit(:password, :email, :admin, :auth_token)
    end

end

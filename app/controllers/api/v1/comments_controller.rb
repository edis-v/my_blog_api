class Api::V1::CommentsController < ApplicationController
  include Collectionable
  skip_before_action :authenticate, only: [:index, :show]
  load_and_authorize_resource :post
  load_and_authorize_resource :comment, through: :post

  resource_description do
    formats [:json]
    short 'API for managing comments'
    error 404, 'Resource not found'
    error 422, 'Unprocessable entity'
    error 401, 'Not authorized for this action'
  end

  def_param_group :comment do
    param :comment, Hash, required: true, desc: 'Comment information' do
      param :body, String, desc: 'Comment content', required: true
    end
  end

  api! 'Get all comments'
  description 'API for fetching all comments'
  param :page, Fixnum, desc: 'Page parameter for pagination. Default value: 1'
  param :per_page, Fixnum, desc: 'How many results will be returned with every page. Default value: 5'
  param :sort, String, desc: 'Sorting records based on any User attribute. \'-\' sign before attr for DESC order.'
  param :fields, String, desc: 'Which user attributes API will return. Divide attributes with \',\' sign.'
  param :id, String, desc: 'Select comment based on attribute value.'
  param :user_id, String, desc: 'Select comments based on attribute value.'
  example "'comment': [{'id':1, 'body':'some text', 'links':'/api/v1/posts/1/comments/1', 'post_id':1, 'user': {...} }, {...}]"
  def index
    @comments = @comments.includes(:owner)
    @comments = work_on_collection(@comments, request.query_parameters)
    @comments = paginate @comments, per_page:  params[:per_page] || 10

    render json: @comments, fields: params[:fields]
  end

  api! 'Get specific comment'
  description 'API for fetching comment with specific ID'
  example "'comment': {'id':1, 'body':'some text', 'links':'/api/v1/posts/1/comments/1', 'post_id':1, 'user': {...} }"

  def show
    render json: @comment, fields: params[:fields]
  end


  api! 'Create comment'
  description <<-EOS
    === Create Comment
      This API is used to create new comment
    ==== Authentication required
      Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example "'comment': {'id':1, 'body':'some text', 'links':'/api/v1/posts/1/comments/1', 'post_id':1, 'user': {...} }"
  param_group :comment
  def create
    @comment.user_id = current_user.id


    if @comment.save
      render json: @comment, status: :created, location: [:api, :v1, @post.user, @post, @comment]
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end


  api! 'Update comment'
  description <<-EOS
    == Update comment
     This API is used to update the comment details.
    === Authentication required
     Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'
  param_group :comment
  def update

    if @comment.update(comment_params)
      head :no_content
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end


  api! 'Delete comment'
  description  <<-EOS
    == Delete comment
     This API is used to delete the comment.
    === Authentication required
     Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'
  def destroy
    @comment.destroy

    head :no_content
  end

  private
    def comment_params
      params.require(:comment).permit(:body, :post_id, :user_id)
    end
end

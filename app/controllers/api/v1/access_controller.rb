class Api::V1::AccessController < ApplicationController

  skip_before_action :authenticate, only: :create

  resource_description do
    formats [:json]
    short 'API for managing user login/logut'
    error 422, 'Unprocessable entity'
  end

  api! 'User login'
  description  'API for user login'
  param :email, String, desc: 'User email', required: true
  param :password, String, desc: 'User password', required: true
  example "'user': {'id':1, 'email':'some@mail.com', 'admin':true, 'links':'api/v1/users/1', 'auth_token':'xxxxxxxx'}"

  def create
    user = User.find_by_email(params[:access][:email])
    if user && user.authenticate(params[:access][:password])
      user.generate_auth_token!
      user.save
      render json: user, serializer: CompleteUserSerializer, root: 'user', status: :ok, location: [:api, :v1, user]
    else
      render json: {errors: 'Invalid email or password'}, status: :unprocessable_entity
    end
  end

  api! 'User logout'
  description <<-EOS
    === User Login
    API for user login
    ==== Authentication required
      Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'

  def destroy
    user = User.find(params[:id])
    user.remove_auth_token!
    user.save
    head 204
  end

  private

  def access_params
    params.require(:access).permit(:password, :email)
  end

end

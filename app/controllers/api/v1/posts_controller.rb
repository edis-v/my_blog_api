class Api::V1::PostsController < ApplicationController
  include Collectionable

  load_and_authorize_resource :user, except: :create
  load_and_authorize_resource :post, :through => :user, except: :create
  authorize_resource only: :create


  resource_description do
    formats [:json]
    short 'API for managing posts'
    error 404, 'Resource not found'
    error 422, 'Unprocessable entity'
    error 401, 'Not authorized for this action'
    #api_versions 'public'
  end

  def_param_group :post do
    param :post, Hash, required: true, desc: 'Post information' do
      param :body, String, desc: 'Post content', required: true
    end
  end


  api! 'Get all posts'
  description 'API for fetching all posts'
  param :page, Fixnum, desc: 'Page parameter for pagination. Default value: 1'
  param :per_page, Fixnum, desc: 'How many results will be returned with every page. Default value: 5'
  param :sort, String, desc: 'Sorting records based on any User attribute. \'-\' sign before attr for DESC order.'
  param :fields, String, desc: 'Which user attributes API will return. Divide attributes with \',\' sign.'
  param :id, String, desc: 'Select post based on attribute value.'
  param :user_id, String, desc: 'Select posts based on attribute value.'
  example "'posts': [{'id':1, 'body':'some text', 'links':'/api/v1/posts/1', 'comment_ids':'[4,5,6]', 'user': {...} }, {...}]"

  def index

    @posts = @posts.includes(:comments)
    @posts = work_on_collection(@posts, request.query_parameters)
    @posts = paginate @posts, per_page: params[:per_page] || 5

    render json: @posts, fields: params[:fields]
  end

  api! 'Get specific post'
  description 'API for fetching posts with specific ID'
  example "'post': {'id':1, 'body':'some text', 'links':'/api/v1/posts/1', 'comment_ids':'[4,5,6]', 'user': {...}"

  def show
    render json: @post, fields: params[:fields]
  end

  api! 'Create post'
  description <<-EOS
    === Create Post
      This API is used to create new post
    ==== Authentication required
      Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example "{'post':{'id':1,'body':'some_text', 'links':'/api/v1/posts/1', 'comment_ids':'[4,5,6]', 'user':{...}"
  param_group :post

  def create
    @post = current_user.posts.new(post_params)
    if @post.valid_user_param?(params[:user_id]) && @post.save
      render json: @post, status: :created, location: [:api, :v1, current_user, @post]
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  api! 'Update post'
  description <<-EOS
    == Update post
     This API is used to update the post details.
    === Authentication required
     Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'
  param_group :post

  def update

    if @post.valid_user_param?(params[:user_id]) && @post.update(post_params)
      head :no_content
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  api! 'Delete post'
  description  <<-EOS
    == Delete post
     This API is used to delete the post.
    === Authentication required
     Authentication token has to be passed as part of the request. It can be passed as HTTP header(AUTHORIZATION).
  EOS
  example 'no_content'

  def destroy
    @post.destroy

    head :no_content
  end

  private

    def post_params
      params.require(:post).permit(:body, :user_id)
    end


end

class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::ImplicitRender
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include ActionController::Helpers
  include ActionController::Instrumentation
  include ActionController::Serialization
  include CanCan::ControllerAdditions
  before_action :authenticate

  def current_user
    authenticate_token
  end

  def authenticate
    authenticate_token || render_unauthorize
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      @current_user ||= User.find_by(auth_token: token)
    end
  end

  def render_unauthorize
    self.headers['WWW-Authenticate'] = 'Token realm = "Application"'
    render json: {message: 'Bad Credentials'}, status: :unauthorized
  end

  rescue_from CanCan::AccessDenied do |exception|
    render json: {message: exception.message}, status: :unauthorized
  end

end

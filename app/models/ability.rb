class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new  # not signed_in user

    alias_action :create, :read, :update, :destroy, :to => :crud
    alias_action :create, :update, :destroy, :to => :cud

    if user.new_record?
      can :read, User
      can :create, User, :admin => false
      can :read, Post
      can :read, Comment
    else
      if user.admin?
        can :crud, :all
        #can :crud, User
        #can :crud, Post
        #can :crud, Comment
      else
        can :create, User, :admin => false
        can :read, User
        can :update, User, :id => user.id, :admin => false
        can :read, Post
        can :cud, Post, :user_id => user.id  #post owner
        can :read, Comment
        can :cud, Comment, :user_id => user.id #comment owner
        can :destroy, Comment, :post => { :user_id => user.id}  #post owner
      end
    end

  end
end

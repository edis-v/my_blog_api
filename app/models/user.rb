class User < ActiveRecord::Base
  has_secure_password
  before_create :generate_auth_token!

  has_many :posts
  has_many :comments, through: :posts
  has_many :own_comments, foreign_key: :user_id, class_name: 'Comment'

  validates_presence_of :email
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i


  def remove_auth_token!
    self.auth_token = nil
  end


  def generate_auth_token!
    begin
      self.auth_token = SecureRandom.urlsafe_base64
    end while self.class.exists?(auth_token: auth_token)
  end


end

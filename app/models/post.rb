class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy

  validates_presence_of :body
  validates_presence_of :user_id


  def valid_user_param?(id)
    if self.user_id.to_i == id.to_i
      true
    else
      errors.add(:user_id, 'Not authorized to create or modify posts of another users - wrong URL')
      false
    end
  end

end

class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :owner, foreign_key: :user_id, class_name: 'User'

  validates_presence_of :body
  validates_presence_of :user_id
  validates_presence_of :post_id

end

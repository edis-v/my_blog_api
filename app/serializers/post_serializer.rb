class PostSerializer < ApplicationSerializer
  include Rails.application.routes.url_helpers
  attributes :id, :body, :links, :comment_ids
  has_one :user

  def links
    {self: api_v1_user_post_path(user.id, object.id)}
  end

  def comment_ids
    object.comment_ids
  end

end

class CommentSerializer < ApplicationSerializer
  include Rails.application.routes.url_helpers
  attributes :id, :body, :post_id, :links
  has_one :owner

  def links
    {self: api_v1_user_post_comment_path(object.post.user_id, object.post_id, object.id)}
  end

end

class ApplicationSerializer < ActiveModel::Serializer
  def attributes()
    data = {}
    if serialization_options[:fields].present?
      serialization_options[:fields].split(',').each do |field|
        data[field] = object[field]
      end
    else
      data = super
    end
    data
  end
end
class CompleteUserSerializer < ApplicationSerializer
  include Rails.application.routes.url_helpers
  attributes :id, :email, :admin, :auth_token, :links

  def links
    {self: api_v1_user_path(object.id)}
  end
end
namespace :db do
  task :populate => :environment do
    #require 'populator'

    require 'ffaker'
    [User, Post, Comment].each(&:delete_all)


    10.times do |n|
      User.create!(password: FFaker::Internet.password, email: FFaker::Internet.email)
    end

    User.all.each do |user|
      5.times do |n|
        Post.create!(body: FFaker::HipsterIpsum.paragraph(rand(5..8)), user_id: user.id)
      end
    end

    Post.all.each do |post|
      5.times do |n|
        Comment.create!(body: FFaker::HipsterIpsum.phrase, post_id: post.id, user_id: rand(User.first.id..User.last.id))
      end
    end

    puts 'Seeding database completed'









  end
end

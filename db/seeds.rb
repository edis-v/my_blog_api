# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[Comment, Post, User].each(&:delete_all)

u1 = User.create(password: 'password', email: 'user@mail.com')
u2 = User.create(password: 'password2', email: 'user2@mail.com')

p1 = u1.posts.create(body: 'Post about Rails app')
p2 = u1.posts.create(body: 'Post about Java app')

p3 = u2.posts.create(body: 'Post about Web development')
p4 = u2.posts.create(body: 'Post about Mobile development')

c1 = p1.comments.create(body: 'Great article', user_id: u2.id)
c2 = p2.comments.create(body: 'Fine article', user_id: u2.id)

c3 = p3.comments.create(body: 'Great WEb dev article', user_id: u1.id)
c4 = p4.comments.create(body: 'I am not mobile dev fan', user_id: u1.id)

